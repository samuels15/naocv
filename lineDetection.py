#!/usr/bin/python
# -*- coding: <encoding name> -*-

from basicf import imshow, drawLine, countLines, csc, cot, delimitKeyPoints
from math import sqrt

import cv2
import numpy as np

def lineAvgPrepare(line1, line2):
    return ((line1[0]+line2[0], line1[1]+line2[1], line1[2]+line2[2]));

def lineSimilarity(line1, line2, rho_threshold=45, theta_threshold=0.3):
    # Para determinacao de linhas similares, temos thresholds para rho e theta
    # que definem os limites para que as linhas sejam consideradas iguais.
    if len(line1)==3 and len(line2)==3: # Considerando linhas que estao sendo agrupadas e tem rho, theta e fator de agrupamento (definido inicialmente como 1 no groupLines)
        if abs(line1[1]/line1[2]-line2[1]/line2[2])<theta_threshold and abs(line1[0]/line1[2]-line2[0]/line2[2] <rho_threshold):
            return True;
        else:
            return False;
    else:               # Considerando linhas que tenham soh rho e theta.
        if abs(line1[1]-line2[1])<=theta_threshold and abs(line1[0]-line2[0]) <=rho_threshold:
            return True;
        else:
            return False;

def groupLines(lines, rho_threshold=45,theta_threshold=0.3):
    new_lines=[]
    for line in lines:
        new_lines.append((line[0],line[1],1));
    lines = new_lines;

    grouped_flag=True;

    while (grouped_flag):
        grouped_flag=False;
        tam=len(lines)
        i=0;
        while i<tam:
            j=i+1;
            while j<tam:
                if (lineSimilarity(lines[i], lines[j],rho_threshold, theta_threshold)):
                    lines[i]= lineAvgPrepare(lines[i],lines[j]);
                    lines.remove(lines[j])
                    j-=1
                    tam-=1;
                    grouped_flag=True;
                j+=1
            i+=1;
    new_lines=[];
    for line in lines:
        new_lines.append((line[0]/line[2], line[1]/line[2]));
    lines=new_lines;

    return (lines)

def getLines(img, threshold=100):
    # Idealmente, a img tem que ser B&W. Entao vamos testar se ela ja veio assim
    # Se nao, eh preciso converter.

    if len(img.shape)==3:
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY);
    else:
        gray = img.copy();
    # Removendo linhas muito finas e ruidos genericos
    gray = cv2.morphologyEx(gray, cv2.MORPH_OPEN, np.ones((1,1),np.uint8))
    # imshow(gray)
    # Apos a conversao, aplicamos um HoughLines.
    edges = cv2.Canny(gray,50,150);
    lines = cv2.HoughLines(edges,1,np.pi/180,threshold)

    # Linhas detectadas como np.ndarray.
    # Passando-as para list.
    new_lines=[]
    if lines is not None:
        for line in lines:
            new_lines.append((line[0][0],line[0][1]));
    else:
        return []

    return new_lines

def drawIntersections(img, intsecs):
    for intsec in intsecs:
        for x, y, type in intsec:
            cv2.circle(img, (x,y), 5, (255,0,0), 1)  # Desenhando a intersecao em azul

def linesDiff(fieldLines,delimiterLines):
    for dLine in delimiterLines:
        tam = len(fieldLines)
        j=0;
        while j<tam:
            fLine=fieldLines[j];
            # Esse rho e theta threshold sao ajustados separadamente para eliminacao das linhas que delimitam o campo.
            # Por isso nao sao os mesmos das linhas similares que estao juntas.
            if (lineSimilarity(dLine, fLine,rho_threshold=3,theta_threshold=0.5)):
                fieldLines.remove(fLine);
                tam-=1;
                continue;
            j+=1
    return fieldLines
