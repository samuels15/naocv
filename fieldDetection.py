import cv2, numpy as np
from basicf import imshow

def green_chromacity(img):
    # Funcao para calcular, pixel a pixel, a proporcao de verde de uma imagem.
    ret = np.zeros((img.shape[0], img.shape[1], 1), np.uint8);

    rows = img.shape[0]
    cols = img.shape[1]

    for i in range(rows):
        for j in range(cols):
            try:
                # ret[i,j] = 255*np.divide(img[i,j][1],np.sum(img[i,j]))
                ret[i,j] = 255*img[i,j][1]/sum(img[i,j])

            except:
                ret[i,j] = 0
    return ret;

def fieldDetector(imgTop, imgBot):
    # roi eh a imagem da camera de baixo cortada considerando um quadrado central
    roi = imgBot[60:200, 80:140]

    # Criando imagem vazia com as dimensoes de imgTop e 1 unico canal:
    g_TopChromacity = np.zeros((imgTop.shape[0], imgTop.shape[1], 1), np.uint8)

    # Processando cromacidade verde para ambas as imgs
    g_BotChromacity = green_chromacity(roi)
    g_TopChromacity = green_chromacity(imgTop)

    # Calculo do histograma considerando a cromacidade verde
    # Com ele, eh possivel determinar o verde mais comum e determinar o menor
    # valor de verde aceitavel. O maximo seria 255 num pixel verde "puro"

    hist = cv2.calcHist([g_BotChromacity],[0],None,[32],[0,256])

    min_g = 0.9*8*(np.where(hist == np.amax(hist)))[0][0]
    max_g = 255

    # Os pixels nao considerados como verdes ficam pretos, os outros ficam brancos.
    # Com a cromacidade encontrada pela imagem inferior, uma transformacao
    # eh aplicada na imagem superior com o threshold

    ret, g_TopChromacity = cv2.threshold(g_TopChromacity,max_g,255,cv2.THRESH_TOZERO_INV);
    ret, g_TopChromacity = cv2.threshold(g_TopChromacity,min_g,255,cv2.THRESH_TOZERO);
    ret, g_TopChromacity = cv2.threshold(g_TopChromacity,0,255,cv2.THRESH_BINARY);

    g_TopChromacity = cv2.erode(g_TopChromacity,np.ones((5,5),np.uint8),iterations = 1)
    g_TopChromacity = cv2.dilate(g_TopChromacity, np.ones((5,5), np.uint8), iterations=1)

    # res = cv2.bitwise_and(imgTop,imgTop,mask=g_TopChromacity)

    # Agora, para encontrar o campo, inspeciona-se cada coluna da imagem
    # Em cada coluna, em uma abordagem top-down, o primeiro pixel verde eh registrado
    # E os que estao abaixo sao "limpos"

    fieldAverage = []
    fieldPixels = []
    rows = imgTop.shape[0]
    cols = imgTop.shape[1]

    for i in range(cols):
        flag=False;         # Flag que indica que ja achou verde na coluna.
        for j in range(rows):
            if (flag):
                g_TopChromacity[j,i]=255;
            elif((g_TopChromacity[j,i]) != 0):     # se o pixel for diferente de 0, indica verde.
                fieldAverage.append(j)
                flag=True;
        fieldPixels.append([i, j])

    # Achando a media do horizonte do campo para fazer uma filtragem
    try:
        fieldAverage = sum(fieldAverage)/len(fieldAverage);
    except:
        fieldAverage = sum(fieldAverage)/(cols+1);

    # Eliminando outliers and calculando a fieldDistance
    fieldBestPixels = []
    fieldDistance = 0
    if (len(fieldPixels)>2):
        for i in range(1, len(fieldPixels)-1):
            if (abs(fieldPixels[i][0]-fieldAverage)<=60):
                fieldBestPixels.append(fieldPixels[i])
                fieldDistance += fieldPixels[i][0]
        fieldDistance = fieldDistance/len(fieldBestPixels)
        #TODO Aqui falta uma linha para atualizar a classe com o horizonHeight = fieldDistance

    # ROI FIELD AND ROI GOAL DETECTION - generate binary image
    rows = imgTop.shape[0]
    cols = imgTop.shape[1]
    roi_field = np.zeros((rows, cols, 1), np.uint8)     # Criando imagem vazia com as dimensoes de imgTop e 1 unico canal
    roi_goal  = np.zeros((rows, cols, 1), np.uint8)     # Criando imagem vazia com as dimensoes de imgTop e 1 unico canal

    if (len(fieldBestPixels)>1):
        for i in range(len(fieldBestPixels)-1):
            if (fieldBestPixels[i+1][1]-fieldBestPixels[i][1]>0):
                a = (fieldBestPixels[i+1][0]-fieldBestPixels[i][0])/(fieldBestPixels[i+1][1]-fieldBestPixels[i][1])
            for j in range (fieldBestPixels[i][1], fieldBestPixels[i+1][1]):
                y_max = a*(j-fieldBestPixels[i][1])+fieldBestPixels[i][0]
                for k in range (rows):
                    if (k>= y_max):
                        roi_field[k,j] = 255
                    if (k<y_max+20 and k>y_max-20):     # Amount of pixels around the edge of roi_field to look for the base of the goal
                        roi_goal[k,j] = 255

    # print ("fieldAverage =", fieldAverage);
    # print ("len(fieldPixels) =", len(fieldPixels))
    # print ("len(fieldBestPixels) =", len(fieldBestPixels));
    # print ("fieldDistance =", fieldDistance)

    # imshow(res, 'Masked Img')   #DEBUG #TODO #REMOVE_LINE
    # imshow(g_TopChromacity, 'g_TopChromacity')   #DEBUG #TODO #REMOVE_LINE

    return g_TopChromacity
    return roi_field, g_TopChromacity, g_BotChromacity

def whiteDetector(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lower_white = np.array([0,0,137], dtype=np.uint8)
    upper_white = np.array([255,132,255], dtype=np.uint8)
    # upper_white = np.array([255,236,255], dtype=np.uint8)
    white_mask = cv2.inRange(hsv, lower_white, upper_white)
    return white_mask;

def testing(name_Top, name_Bot):
    fieldDetector(cv2.imread(name_Top), cv2.imread(name_Bot));

# testing('media/top_and_bottom/apartamento_iluminado_1.jpg', 'media/top_and_bottom/apartamento_iluminado_bottom_1.jpg');
# testing('media/penalti_pic.jpg', 'media/penalti_pic.jpg');
