import pandas as pd
import sys

def similarCoordinate(x1,y1,x2,y2,threshold_x=15,threshold_y=15):
    try:
        if abs(int(x1)-int(x2))<threshold_x and abs(int(y1)-int(y2))<threshold_y:
            return True
        else:
            return False
    except ValueError:
        return False

def allNone(*args):
    if args.count('None') == len(args):
        return True;
    else:
        return False;

def similarFeatureType (type1, type2):
    return type1==type2;

df1 = pd.read_csv(sys.argv[1])           # Log
df2 = pd.read_csv(sys.argv[2])           # Gabarito

df1.sort_values(by=['filename'], inplace=True)
df2.sort_values(by=['filename'], inplace=True)

v = len(df1)*[False];
for idx1, row1 in df1.iterrows():
    found_first=False;
    found_last=False;
    for idx2, row2 in df2.iterrows():
        if row1['filename']==row2['filename']:
            found_first=True;
            # v[idx1]=v[idx1] or (similarFeatureType(row1['feature'], row2['feature']) and similarCoordinate(row1['x'],row1['y'],row2['x'],row2['y'])) or (allNone(row1['x'],row1['y'],row2['x'],row2['y'], row1['feature'],row2['feature']));
            v[idx1]=v[idx1] or (similarCoordinate(row1['x'],row1['y'],row2['x'],row2['y'])) or (allNone(row1['x'],row1['y'],row2['x'],row2['y'], row1['feature'],row2['feature']));
        else:
            if found_first==True:
                found_last=True
        if found_last==True:
            # print (row1['filename'])
            break;
    if found_first==False and allNone(row1['filename'], row1['x'], row1['y']):
        v[idx1]=True

print ('Evaluation :', sum(1 for x in v if x==True)/len(v))

for idx,row in df1.iterrows():
    print (idx, row['filename'], row['x'],row['y'],row['feature'],v[idx])
