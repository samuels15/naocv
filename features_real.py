import cv2
import numpy as np
from basicf import drawLines, clearDirectory, imshow
from fieldDetection import fieldDetector, whiteDetector
from lineDetection import linesDiff, getLines, lineSimilarity, groupLines
from intsecDetection import getIntersections, qualifyPoint, classifyIntersections

from os import listdir, remove
from os.path import isfile, join, isdir, dirname, splitext,basename
import sys
import pandas as pd

def pic_process(folder_path, pic_path, tthres=100, bthres=55, rho_threshold=15, theta_threshold=0.2, rthres=30,radthres=3):
    filename=join(folder_path,pic_path)

    img = cv2.imread(filename)
    fieldMask = fieldDetector(cv2.imread(filename), cv2.imread(join(folder_path,'frame000094.png'))) # Extracao da mascara do campo
    # cv2.imwrite(filename[:-4]+"fieldMask.jpg", fieldMask)
    imgField = cv2.bitwise_and(img,img,mask=fieldMask) # Aplicacao da mascara do campo na imagem original
    # cv2.imwrite(filename[:-4]+"_field.jpg",imgField)
    whiteMask = whiteDetector(imgField)
    whiteMask = cv2.bitwise_and(whiteMask,fieldMask)
    imgWhite = cv2.bitwise_and(imgField,imgField,mask=whiteMask)
    cv2.imwrite(filename[:-4]+"_white.jpg",imgWhite)
    # Agora, vamos extrair as linhas da mascara do campo e extrair as linhas da imagem tratada pos-mascara
    delimiterLines = getLines(fieldMask)
    fieldLines = getLines(imgField,tthres)
    fieldLines = linesDiff (fieldLines, delimiterLines)

    # Vamos remover as linhas encontradas no campo que sao basicamente iguais as linhas que delimitam o campo.
    fieldLines = linesDiff(fieldLines,delimiterLines)
    fieldLines = groupLines(fieldLines,rho_threshold,theta_threshold)
    # Agora, temos as linhas "filtradas".

    crossings = getIntersections(fieldMask,fieldLines,rthres,radthres)
    for intersec in crossings:
        for coord in intersec[1:]:
            if qualifyPoint(whiteMask, coord[0],coord[1],radthres):
                img = cv2.circle(img, coord, 1, (255,0,0), 2)
            else:
                img = cv2.circle(img, coord, 1, (0,0,255), 2)

    intersections = classifyIntersections(whiteMask,crossings,radthres)

    if (len(intersections)>0) and (len(intersections)<10):
        drawLines(img,fieldLines,colour=(0,255,0))
        for intersec in intersections:
            cv2.putText(img,intersec[1],intersec[0],cv2.FONT_HERSHEY_SIMPLEX, 0.2, (255,0,0), 1);
            with open(join(folder_path,'log.txt'),"a") as log_file:
                log_file.write(pic_path+','+str(intersec[0][0])+','+str(intersec[0][1])+','+str(intersec[1])+'\n');
        cv2.imwrite(filename[:-4]+"_lines.jpg",img)
    else:
        with open(join(folder_path,'log.txt'),"a") as log_file:
            log_file.write(pic_path+',None,None,None\n');

def logfilesInspect():
    folder_path='./media/imageset_363'
    clearDirectory(folder_path,erase_keywords=["lines", "white", "fieldMask", "field"]);
    files=listdir(folder_path);
    files=sorted(files)

    with open(join(folder_path,'log.txt'),"w") as log_file:
        log_file.write("filename,x,y,feature\n")
    # Processando arquivos
    count=0
    for filename in files:
        fn = join(folder_path,filename)
        print (filename)
        if isfile(fn) and splitext(fn)[1] in ['.jpg', '.png']:
            pic_process(folder_path,filename);
        else:
            pass;
        count+=1
        if count>300:
            break;

def nonblurredInspect():
    folder_path='./media/imageset_363';
    clearDirectory(folder_path,erase_keywords=["lines", "white", "fieldMask", "field"]);
    df = pd.read_csv("export_2629.txt")

    with open(join(folder_path,'log.txt'), "w") as log_file:
        log_file.write("filename,x,y,feature\n")

    new_files=[]
    for idx,row in df.iterrows():
        if row['x']!='None' and row['y']!='None':
            new_files.append(row['filename']);

    new_files = sorted(list(set(new_files)))
    count=0
    for filename in new_files:
        fn = join(folder_path,filename)
        if isfile(fn) and splitext(fn)[1] in ['.jpg', '.png']:
            print (filename)
            pic_process(folder_path,filename);
        else:
            pass;
        count+=1
        if count>500:
            break;

nonblurredInspect()

# pic_process('./media/imageset_363','frame000115.png')
# pic_process('./media/imageset_363', 'frame000000.png');
