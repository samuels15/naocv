import cv2
import numpy as np
from math import sin,tan, sqrt
from os import listdir, remove
from os.path import isfile, join, splitext

def csc(x):
    if sin(x)!=0:
        return (1/sin(x));
    else:
        return 1e+16;

def cot(x):
    if tan(x)!=0:
        return (1/tan(x));
    else:
        return 1e+16;

def imshow(img, title='Title'):
    cv2.imshow(title, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def get_kernel(n):
    return np.ones((n,n),np.uint8)

def countLines(img, threshold):
    return (len(cv2.HoughLines(img,1,np.pi/180,threshold)))

def drawLines (img, lines, colour=(0,0,255)):
    for line in lines:
        drawLine(img,line[0],line[1],colour=colour);

def drawLine(img, rho, theta, factor=1000, colour=(0,0,255)):
    a = np.cos(theta);
    b = np.sin(theta);
    x0 = a*rho;
    y0 = b*rho;
    x1 = int(x0 + factor*(-b))
    y1 = int(y0 + factor*(a))
    x2 = int(x0 - factor*(-b))
    y2 = int(y0 - factor*(a))
    cv2.line(img,(x1,y1),(x2,y2),colour,2)

def hess2cart(rho,theta):
    return cot(theta),rho*csc(theta)

def delimitKeyPoints(rho, theta, x0, y0, d=10):
    a = cot(theta)
    b = rho*csc(theta)
    y0=-y0
    x1 = (2*x0 + 2*a*y0 - 2*a*b - sqrt((2*a*b-2*x0-2*a*y0)**2 - 4*(1+a**2)*(x0**2 +b**2+y0**2-d**2-2*y0*b)))/(2*(1+a**2))
    x2 = (2*x0 + 2*a*y0 - 2*a*b + sqrt((2*a*b-2*x0-2*a*y0)**2 - 4*(1+a**2)*(x0**2 +b**2+y0**2-d**2-2*y0*b)))/(2*(1+a**2))
    y1 = a*x1+b;
    y2 = a*x2+b;
    return int(x1), int(x2), int(y1), int(y2)

def getFileExt(filename):
    return splitext(filename)[-1];

def clearDirectory(folder_path, formats=['.jpg', '.png'], erase_keywords=["modified"]):
    for f in listdir(folder_path):
        if (isfile(join(folder_path, f)) and getFileExt(f) in formats):
            if any(keyword in f for keyword in erase_keywords):
                remove(join(folder_path,f))
