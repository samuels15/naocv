# Identificação de features de campo com OpenCV para uso em robô NAO
Código desenvolvido no contexto de Trabalho de Graduação que foi submetido como requisito parcial para obtenção do grau de Engenheiro de Controle e Automação.

-------------

## Motivação
Competições de futebol de robô ocorrem na RoboCup. Dividida em ligas, a Standard Platform League utiliza o robô NAO. A motivação para o desenvolvimento desse código foi permitir a identificação de features de campo para que possam ser utilizadas na melhoria da estratégia de jogo e refinamento da localização de um robô em campo.

------------------------
## Requisitos
* [Python](https://www.python.org) instalado (Funciona nas versões 2 e 3).
* ```pip``` instalado (normalmente vem junto com Python)
* Instalação de bibliotecas do Python
    * cv2, numpy
    * Para os códigos auxiliares, é preciso ainda instalar a biblioteca pandas.

------------------
## Passo a passo
Os arquivos **fieldDetection.py**, **lineDetection.py** e **intsecDetecion.py** têm as funções para detecção de campo, linhas e interseções, respectivamente.
O arquivo **basicf.py** têm funções básicas usadas nesse trabalho, como as funções trigonométricas cossecante e cotangente, bem como uma função para exibição da imagem (e essa especificamente só funciona com Python3).

Os arquivos **features_real.py** e **features_sim.py** foram utilizados para obtenção dos resultados do trabalho de graduação, com base em imagens que devem ficar em pastas nos caminhos ./media/simulation/ e ./media/imageset_363/. Embora sejam arquivos diferentes devido à diferença das fontes e dos parâmetros, a mesma metodologia é seguida em ambos.
* Carregamento imagem a imagem;
* Segmentação do campo;
* Detecção de linhas
* Detecção de interseções
* Classificação de interseções
* Exportar interseções classificadas e filtradas para arquivo texto.

A comparação das features se baseia num gabarito construído manualmente, com base nas figuras.

------------------
## Comparação

O gabarito e os registros salvos estão no formato .txt, mas internamente se assemelham a CSVs, em que se separam em 4 colunas:

filename, x, y, feature.

O código de comparação **compare.py** é executado e usa a biblioteca pandas para avaliar a efetividade da detecção.
------------------
## Resultados
Este código obteve uma taxa de 62% de acerto com um grupo de imagens de simulação (oriundas do simulador VREP) e uma taxa de 21% de acerto em imagens reais de datasets retirados da plataforma ImageTagger.
Os gabaritos e os arquivos oriundos da comparação estão na pasta **gabarito**.
