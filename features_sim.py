import cv2
import numpy as np
from basicf import drawLines, clearDirectory, imshow
from fieldDetection import fieldDetector, whiteDetector
from lineDetection import linesDiff, getLines, lineSimilarity, groupLines
from intsecDetection import getIntersections, qualifyPoint, classifyIntersections

from os import listdir, remove
from os.path import isfile, join, isdir, dirname, splitext,basename
import sys
import pandas as pd

def double_pic_process(bpic_path, tpic_path, tthres=55, rthres=30, radthres=5):
    if isfile(bpic_path) and isfile(tpic_path):
        bimg=cv2.imread(bpic_path);     # Carrega a img bottom
        timg=cv2.imread(tpic_path);     # Carrega a img top
        topFieldMask = fieldDetector(timg,bimg);   # Extraindo o campo com a combinacao das imgs
        # cv2.imwrite(tpic_path[:-4]+"_fieldMask.jpg",topFieldMask); # Gravando a mascara
        topImgField=cv2.bitwise_and(timg, timg,mask=topFieldMask);   # Aplicando a mascara
        topWhiteMask = whiteDetector(topImgField)
        topWhiteMask = cv2.bitwise_and(topWhiteMask,topFieldMask);
        topImgWhite = cv2.bitwise_and(topImgField,topImgField, mask=topWhiteMask);
        # cv2.imwrite(tpic_path[:-4]+"_field.jpg",topImgField);   # Salvando a img com a mascara de campo aplicada
        # cv2.imwrite(tpic_path[:-4]+"_white.jpg",topImgWhite);   # Salvando a img com a mascara branca aplicada
        topImgDelimiterLines = getLines(topFieldMask)   # Extraindo linhas delimitadoras a partir da mascara
        # drawLines(timg,topImgDelimiterLines,colour=(255,0,255));
        topImgFieldLines = getLines(topImgField,tthres) # Extraindo linhas da img com a mascara
        fieldLines = linesDiff(topImgFieldLines,topImgDelimiterLines) # Deletando linhas similares entre as selecoes
        # drawLines(timg,fieldLines,colour=(255,255,0));
        fieldLines = groupLines(fieldLines, rho_threshold=45, theta_threshold=0.3);
        # Desenhando as linhas delimitadoras na img original
        # drawLines(timg,topImgDelimiterLines,colour=(0,0,255));
        # Desenhando as linhas do campo na img original
        # imshow(timg)
        topIntersections = getIntersections(topFieldMask,fieldLines,rthres,radthres)
        topIntersections = classifyIntersections(topWhiteMask, topIntersections, radthres)

        if len(topIntersections)>0:
            drawLines(timg,fieldLines,colour=(0,255,0))
            for intersec in topIntersections:
                cv2.putText(timg,intersec[1],intersec[0],cv2.FONT_HERSHEY_SIMPLEX, 0.2, (255,0,0), 1);
                with open(join(dirname(tpic_path),'log.txt'),"a") as log_file:
                    log_file.write(basename(tpic_path)+','+str(intersec[0][0])+','+str(intersec[0][1])+','+str(intersec[1])+'\n');
            cv2.imwrite(tpic_path[:-4]+"_lines.jpg", timg)
        else:
            with open(join(dirname(tpic_path),'log.txt'),"a") as log_file:
                log_file.write(basename(tpic_path)+',None,None,None\n');

def doubleInspection():
    folder_path='./media/simulation/'
    clearDirectory(folder_path,erase_keywords=["fieldMask","lines", "white"])
    df = pd.read_csv("simulation_log.txt")
    files = df.filename.unique()
    new_files=[]

    # Limpando o arquivo de log e colocando cabecalho.
    with open(join(folder_path,'log.txt'),"w") as log_file:
        log_file.write("filename,x,y,feature\n")

    for filename in files:
        if filename[0]=='t':
            new_files.append(filename[1:])

    new_files = sorted(new_files)

    # count=0
    for filename in new_files:
        print (filename)
        bname = join(folder_path, 'b'+filename)
        tname = join(folder_path, 't'+filename)
        double_pic_process(bname, tname)
        # count+=1
        # if count>=20:
        #     break;
        pass

doubleInspection();

# double_pic_process("./media/simulation/bframe0009.jpg", "./media/simulation/tframe0009.jpg")
# double_pic_process("./media/simulation/bframe0010.jpg", "./media/simulation/tframe0010.jpg")
