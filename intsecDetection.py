from basicf import csc, cot
import numpy as np
from math import sqrt

def getIntersections(bin_img,lines,r=50,radius_threshold=3):
    # intsecs = []
    control = []
    height,width = bin_img.shape[:2]

    for i in range(len(lines)):
        rho1,theta1 = lines[i]
        for j in range(i+1,len(lines)):
            feat_type = [];
            rho2,theta2 = lines[j]
            x = (rho1*csc(theta1)-rho2*csc(theta2))/(cot(theta2)-cot(theta1))
            y = (cot(theta2)*rho1*csc(theta1)-cot(theta1)*rho2*csc(theta2))/(cot(theta2)-cot(theta1))
            # print (x,y)
            if x == float("inf") or x == float("-inf") or y == float("inf") or y == float("-inf"):
                x = 1;
                y = -1;

            if (x<=0) and (y>=0) and (abs(x)<width) and (abs(y)<height):
                x = int(abs(x));
                y = int(abs(y));
                controlpoint1=(int(x-r*np.sin(theta1)),int(y+r*np.cos(theta1)));
                controlpoint2=(int(x+r*np.sin(theta1)),int(y-r*np.cos(theta1)));
                controlpoint3=(int(x-r*np.sin(theta2)),int(y+r*np.cos(theta2)));
                controlpoint4=(int(x+r*np.sin(theta2)),int(y-r*np.cos(theta2)));
                # feat_type.append(qualifyPoint(bin_img,controlpoint1[0],controlpoint1[1], radius_threshold))
                # feat_type.append(qualifyPoint(bin_img,controlpoint2[0],controlpoint2[1], radius_threshold))
                # feat_type.append(qualifyPoint(bin_img,controlpoint3[0],controlpoint3[1], radius_threshold))
                # feat_type.append(qualifyPoint(bin_img,controlpoint4[0],controlpoint4[1], radius_threshold))
                if proximityCheck(controlpoint1,controlpoint2,controlpoint3,controlpoint4):
                    if bin_img[y,x]==255:
                        # intsecs.append((x,y, featureType(feat_type)))
                        control.append(((x,y),controlpoint1,controlpoint2,controlpoint3,controlpoint4))
    # return intsecs ,control;
    return control

def classifyIntersections(bin_img,intsecs, radius_threshold=3):
    if intsecs is not None:
        valid=[]
        for intsec in intsecs:
            center_white =  qualifyPoint(bin_img, intsec[0][0],intsec[0][1],radius_threshold)
            cp1_white    =  qualifyPoint(bin_img, intsec[1][0],intsec[1][1],radius_threshold)
            cp2_white    =  qualifyPoint(bin_img, intsec[2][0],intsec[2][1],radius_threshold)
            cp3_white    =  qualifyPoint(bin_img, intsec[3][0],intsec[3][1],radius_threshold)
            cp4_white    =  qualifyPoint(bin_img, intsec[4][0],intsec[4][1],radius_threshold)
            count = sum (1 for ind in [cp1_white,cp2_white,cp3_white,cp4_white] if ind == True)
            if count == 2:
                if (cp1_white and cp2_white) or (cp3_white and cp4_white):
                    continue;
            if (2<=count<=4):
                valid.append((intsec[0],featureType(count)))
        return valid
    else:
        return []

def qualifyPoint(bin_img, x, y, radius_threshold=3):
    # Funcao que, dado um ponto x, y e uma imagem, determina se existe
    # um ponto 0 nas redondezas (determinadas pelo radius_threshold)
    # print()
    for i in range(x-radius_threshold,x+radius_threshold):
        for j in range(y-radius_threshold,y+radius_threshold):
            # if (bin_img[j,i]==0) # A linha parou de ser essa para respeitar os limites da img.
            try:
                # print ((i,j),bin_img[min(j,bin_img.shape[0]-1),min(i,bin_img.shape[1]-1)])
                if (bin_img[min(j,bin_img.shape[0]-1),min(i,bin_img.shape[1]-1)]==255):
                    return True;
            except ValueError:
                pass;
    return False;

def featureType(count):
    if count==2:
        return 'L';
    elif count==3:
        return 'T';
    elif count==4:
        return 'X';
    else:
        return '?';

def closePoints(coord1, coord2, threshold):
    x1, y1 = coord1
    x2, y2 = coord2
    distance = sqrt((x1-x2)**2 + (y1-y2)**2)
    if distance < threshold:
        return True;
    else:
        return False;

def proximityCheck(point1, point2, point3, point4, threshold=4):
    # Funcao que determina se a intersecao eh valida ou nao
    # conforme proximidade dos seus pontos de controle
    # Se eles forem muito perto um do outro, essa intersecao sera descartada
    # Esse threshold eh o fator r do getIntersections dividido por 10.
    if closePoints(point1, point3, threshold):
        return False;
    elif closePoints(point1, point4, threshold):
        return False;
    else:
        return True;
